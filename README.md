- se non esiste la cartella /tmp/virtualhosts crearla  

- clonare la repository dei virtualhosts nella cartella /tmp/virtualhosts

- creare / modificare i virtualhosts presenti nella cartella /tmp/virtualhosts/active
	- creare l'utente e il gruppo
	- creare la directory
	- creare il virtualhost
	- creare il db
	- creare il certificato per l'https

- eliminare i virtualhosts presenti nella cartella /tmp/virtualhosts/disabled
	- eliminare il virtualhost
	- eliminare la directory	
	- eliminare il db
	- eliminare il certificato per l'https
	- eliminare l'utente e il gruppo

- ripulire la cartella /tmp/virtualhosts

Per aggiornare i virtualhost sul server fare l'accesso al server con

```
ssh root@IP_DEL_SERVER
```

da qui eseguire il comando

```
ansible-playbook vh-manager/roger-vh-manager.yml
```
